<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="author" content="Manuel Meyer">
    <meta name="description" content="Manuel Meyer&#39;s website">
    <meta name="keywords" content="blog,developer,personal">

    <meta name="twitter:card" content="summary"/>
<meta name="twitter:title" content="Immutable Types in Swift"/>
<meta name="twitter:description" content="Object-Orientation and Functional Programming are different solutions to the same problem: Dealing with state in complex systems. Both had identified global mutable state as the source of recurring issues and headaches.
While OOP tackles that problem by hiding mutable state inside of objects and this state should be only accessible through the object&rsquo;s public interface, FP doesn&rsquo;t hide the state, rather discourages mutable state. Effectively OOP deleted &ldquo;global&rdquo; from &ldquo;global mutable state&rdquo; while FP erased &ldquo;mutable&rdquo;."/>

    <meta property="og:title" content="Immutable Types in Swift" />
<meta property="og:description" content="Object-Orientation and Functional Programming are different solutions to the same problem: Dealing with state in complex systems. Both had identified global mutable state as the source of recurring issues and headaches.
While OOP tackles that problem by hiding mutable state inside of objects and this state should be only accessible through the object&rsquo;s public interface, FP doesn&rsquo;t hide the state, rather discourages mutable state. Effectively OOP deleted &ldquo;global&rdquo; from &ldquo;global mutable state&rdquo; while FP erased &ldquo;mutable&rdquo;." />
<meta property="og:type" content="article" />
<meta property="og:url" content="/posts/immutable-types-in-swift/" />
<meta property="article:published_time" content="2020-05-01T00:00:00+00:00" />
<meta property="article:modified_time" content="2020-05-01T00:00:00+00:00" />



    
      <base href="/posts/immutable-types-in-swift/">
    
    <title>
  Immutable Types in Swift · Manuel Meyer
</title>

    
      <link rel="canonical" href="/posts/immutable-types-in-swift/">
    

    <link href="https://fonts.googleapis.com/css?family=Fira+Code:wght@3007CLato:400,700%7CSpectral:400,700%7CSource+Code+Pro:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" integrity="sha384-KA6wR/X5RY4zFAHpv/CnoG2UW1uogYfdnP67Uv7eULvTveboZJg0qUpmJZb5VqzN" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha256-l85OmPOjvil/SOvVt3HnSSjzF1TUMyT9eV0c2BzEGzU=" crossorigin="anonymous" />

    
      
      
      <link rel="stylesheet" href="/css/coder.min.8d728e796e1a7754a836cf9b1285a128dca6308708edbfe4cf66a64bd69ff6f7.css" integrity="sha256-jXKOeW4ad1SoNs&#43;bEoWhKNymMIcI7b/kz2amS9af9vc=" crossorigin="anonymous" media="screen" />
    

    

    

    

    

    

    <link rel="icon" type="image/png" href="/images/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/images/favicon-16x16.png" sizes="16x16">

    <meta name="generator" content="Hugo 0.73.0" />
  </head>

  
  
  <body class="colorscheme-light">
    <main class="wrapper">
      <nav class="navigation">
  <section class="container">
    <a class="navigation-title" href="/">
      Manuel Meyer
    </a>
    
    <input type="checkbox" id="menu-toggle" />
    <label class="menu-button float-right" for="menu-toggle"><i class="fas fa-bars"></i></label>
    <ul class="navigation-list">
      
        
          <li class="navigation-item">
            <a class="navigation-link" href="/about/">About</a>
          </li>
        
          <li class="navigation-item">
            <a class="navigation-link" href="/posts/">Blog</a>
          </li>
        
          <li class="navigation-item">
            <a class="navigation-link" href="/contact/">Contact me</a>
          </li>
        
      
      
    </ul>
    
  </section>
</nav>


      <div class="content">
        
  <section class="container post">
    <article>
      <header>
        <div class="post-title">
          <h1 class="title">Immutable Types in Swift</h1>
        </div>
        <div class="post-meta">
          <div class="date">
            <span class="posted-on">
              <i class="fas fa-calendar"></i>
              <time datetime='2020-05-01T00:00:00Z'>
                May 1, 2020
              </time>
            </span>
            <span class="reading-time">
              <i class="fas fa-clock"></i>
              8-minute read
            </span>
          </div>
          
          <div class="tags">
  <i class="fas fa-tag"></i>
    <a href="/tags/architecture/">Architecture</a></div>

        </div>
      </header>

      <div>
        
        <p>Object-Orientation and Functional Programming are different solutions to the same problem: Dealing with state in complex systems. Both had identified global mutable state as the source of recurring issues and headaches.</p>
<p>While OOP tackles that problem by hiding mutable state inside of objects and this state should be only accessible through the object&rsquo;s public interface, FP doesn&rsquo;t hide the state, rather discourages mutable state.
Effectively OOP deleted &ldquo;global&rdquo; from &ldquo;global mutable state&rdquo; while FP erased &ldquo;mutable&rdquo;.</p>
<p>For many years these solutions had been equivalent, and it is fair to say that OOP dominated the scene while FP slowly but steadily grew its discipleship in niche markets like high performance computing and telecommunications.</p>
<p>Then multicore machines became widely available — and that changed everything. Suddenly one of the solutions became better than the other.
While functional programs pretty much were able to use any number of cores simply because of the absence of possible side effects created by the hidden state, concurrency is an issue not generally solved for OOP.
Different OOP systems offer different solution, like threads and (purpose defeating) locks, dispatching, value over reference types, favouring enumerations of collections over plain iterating.</p>
<p>Well, I think another solution in OOP can be: Copying FP&rsquo;s solution.</p>
<h2 id="this-is-a-proposal-for-using-fully-immutable-datatypes-in-swift">This is a proposal for using fully immutable datatypes in Swift</h2>
<p>By &ldquo;immutable state&rdquo; we mean that once an object is initialized, its contents cannot be changed; rather if a new value need to be reflected, a new object is created from the old one with the value being replaced.</p>
<p>Let&rsquo;s look at some example:</p>
<div class="highlight"><pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-swift" data-lang="swift"><span style="color:#00a">struct</span> <span style="color:#0a0;text-decoration:underline">TodoItem</span> {
    <span style="color:#00a">init</span>(text: <span style="color:#0aa">String</span>) {
        <span style="color:#00a">self</span>.<span style="color:#00a">init</span>(text: text, completed: <span style="color:#00a">false</span>, dueDate: <span style="color:#00a">nil</span>, creationDate: Date())
    }

    <span style="color:#00a">private</span>
    <span style="color:#00a">init</span>(text: <span style="color:#0aa">String</span>, completed: <span style="color:#0aa">Bool</span>, dueDate: Date?, creationDate: Date) {
        <span style="color:#00a">self</span>.text = text
        <span style="color:#00a">self</span>.completed = completed
        <span style="color:#00a">self</span>.dueDate = dueDate
        <span style="color:#00a">self</span>.creationDate = creationDate
    }
    <span style="color:#00a">let</span> <span style="color:#a00">text</span>: <span style="color:#0aa">String</span>
    <span style="color:#00a">let</span> <span style="color:#a00">completed</span>: <span style="color:#0aa">Bool</span>
    <span style="color:#00a">let</span> <span style="color:#a00">dueDate</span>: Date?
    <span style="color:#00a">let</span> <span style="color:#a00">creationDate</span>: Date

    <span style="color:#00a">func</span> <span style="color:#0a0">set</span>(text:<span style="color:#0aa">String</span>)    -&gt; TodoItem { TodoItem(text: text, completed: completed, dueDate: dueDate, creationDate: creationDate) }
    <span style="color:#00a">func</span> <span style="color:#0a0">set</span>(completed:<span style="color:#0aa">Bool</span>) -&gt; TodoItem { TodoItem(text: text, completed: completed, dueDate: dueDate, creationDate: creationDate) }
    <span style="color:#00a">func</span> <span style="color:#0a0">set</span>(dueDate:Date)   -&gt; TodoItem { TodoItem(text: text, completed: completed, dueDate: dueDate, creationDate: creationDate) }
}</code></pre></div>
<p>We see a todo item that will be created with some text given by the user and some default values. All properties are immutable. To create new objects with changed values, you have to use the setter — which in contrary to &ldquo;normal&rdquo; OOP doesn&rsquo;t change a value in an existing object, but returns a new object reflecting the changes. If you wanted to create an item that has an due date and is completed, you would do something like:</p>
<div class="highlight"><pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-swift" data-lang="swift"><span style="color:#00a">let</span> <span style="color:#a00">t</span> = TodoItem(text: <span style="color:#a50">&#34;Buy Beer&#34;</span>).<span style="color:#00a">set</span>(dueDate: Date.tomorrow.noon).<span style="color:#00a">set</span>(completed: <span style="color:#00a">true</span>)</code></pre></div>
<p>There is one thing I don&rsquo;t like much: Depending on the data provided by the user, a different method has to be called, pushing a lot of responsibility into the ui layer.</p>
<p>Instead I want to propose this form:</p>
<div class="highlight"><pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-swift" data-lang="swift"><span style="color:#00a">struct</span> <span style="color:#0a0;text-decoration:underline">TodoItem</span>: <span style="color:#0aa">Equatable</span>, <span style="color:#0aa">Hashable</span> {

    <span style="color:#00a">enum</span> <span style="color:#0a0;text-decoration:underline">Change</span> {
        <span style="color:#00a">case</span> text(<span style="color:#0aa">String</span>)
        <span style="color:#00a">case</span> completed(<span style="color:#0aa">Bool</span>)
        <span style="color:#00a">case</span> id(UUID)
        <span style="color:#00a">case</span> due(Date?)
    }

    <span style="color:#00a">let</span> <span style="color:#a00">text</span>: <span style="color:#0aa">String</span>
    <span style="color:#00a">let</span> <span style="color:#a00">completed</span>: <span style="color:#0aa">Bool</span>
    <span style="color:#00a">let</span> <span style="color:#a00">id</span>: UUID
    <span style="color:#00a">let</span> <span style="color:#a00">dueDate</span>: Date?
    <span style="color:#00a">let</span> <span style="color:#a00">alterDate</span>: Date?
    <span style="color:#00a">let</span> <span style="color:#a00">creationDate</span>: Date

    <span style="color:#00a">init</span>(text: <span style="color:#0aa">String</span>) {
        <span style="color:#00a">self</span>.<span style="color:#00a">init</span>(text, <span style="color:#00a">false</span>, UUID(), <span style="color:#00a">nil</span>, Date(), <span style="color:#00a">nil</span>)
    }

    <span style="color:#00a">func</span> <span style="color:#0a0">alter</span>(<span style="color:#00a">_</span> changes: Change...) -&gt; TodoItem { changes.reduce(<span style="color:#00a">self</span>) { <span style="color:#a00">$0</span>.alter(<span style="color:#a00">$1</span>) } }

<span style="color:#aaa;font-style:italic">// </span><span style="color:#00a;font-style:italic">MARK:</span><span style="color:#aaa;font-style:italic"> - private</span>

    <span style="color:#00a">private</span> <span style="color:#00a">init</span> (<span style="color:#00a">_</span> text: <span style="color:#0aa">String</span>, <span style="color:#00a">_</span> completed: <span style="color:#0aa">Bool</span>, <span style="color:#00a">_</span> id: UUID, <span style="color:#00a">_</span> dueDate:Date?, <span style="color:#00a">_</span> creationDate: Date, <span style="color:#00a">_</span> alterDate:Date?) {
        <span style="color:#00a">self</span>.text = text
        <span style="color:#00a">self</span>.completed = completed
        <span style="color:#00a">self</span>.id = id
        <span style="color:#00a">self</span>.creationDate = creationDate
        <span style="color:#00a">self</span>.dueDate = dueDate
        <span style="color:#00a">self</span>.alterDate = alterDate
    }

    <span style="color:#00a">private</span> <span style="color:#00a">func</span> <span style="color:#0a0">alter</span>(<span style="color:#00a">_</span> change:Change) -&gt; TodoItem {
        <span style="color:#00a">let</span> <span style="color:#a00">alterDate</span> = Date()
        <span style="color:#00a">switch</span> change {
        <span style="color:#00a">case</span> .text     (<span style="color:#00a">let</span> <span style="color:#a00">text</span>     ): <span style="color:#00a">return</span> TodoItem( text, completed, id, dueDate, creationDate, alterDate )
        <span style="color:#00a">case</span> .completed(<span style="color:#00a">let</span> <span style="color:#a00">completed</span>): <span style="color:#00a">return</span> TodoItem( text, completed, id, dueDate, creationDate, alterDate )
        <span style="color:#00a">case</span> .id       (<span style="color:#00a">let</span> <span style="color:#a00">id</span>       ): <span style="color:#00a">return</span> TodoItem( text, completed, id, dueDate, creationDate, alterDate )
        <span style="color:#00a">case</span> .due      (<span style="color:#00a">let</span> <span style="color:#a00">dueDate</span>  ): <span style="color:#00a">return</span> TodoItem( text, completed, id, dueDate, creationDate, alterDate )
        }
    }
}</code></pre></div>
<p>Here we don&rsquo;t use a setter for each value, but a method &ldquo;alter&rdquo; that takes any number of Change request, which are enums with associated values.</p>
<div class="highlight"><pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-swift" data-lang="swift"><span style="color:#00a">let</span> <span style="color:#a00">t</span> = TodoItem(text: <span style="color:#a50">&#34;Buy Beer&#34;</span>).<span style="color:#00a">set</span>(dueDate: Date.tomorrow.noon).<span style="color:#00a">set</span>(completed: <span style="color:#00a">true</span>)</code></pre></div>
<p>would become</p>
<div class="highlight"><pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-swift" data-lang="swift"><span style="color:#00a">let</span> <span style="color:#a00">t</span> = TodoItem(text: <span style="color:#a50">&#34;Buy Beer&#34;</span>).alter(.due(Date.tomorrow.noon), .completed(<span style="color:#00a">true</span>))</code></pre></div>
<p>This has one issue, though: to call <code>alter(_ changes: Change...) -&gt; TodoItem</code> we must manage the user selection for certain values till everything is in place and <code>alter(_ changes: )</code> can be called. Wouldn&rsquo;t it be nice if we just could store all values in an array and send that list to <code>alter(_ changes: )</code>? Sure it would!</p>
<p>We change
<div class="highlight"><pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-swift" data-lang="swift"><span style="color:#00a">func</span> <span style="color:#0a0">alter</span>(<span style="color:#00a">_</span> changes: Change...) -&gt; TodoItem { alter(changes) }
<span style="color:#00a">func</span> <span style="color:#0a0">alter</span>(<span style="color:#00a">_</span> changes: [Change] ) -&gt; TodoItem { changes.reduce(<span style="color:#00a">self</span>) { <span style="color:#a00">$0</span>.alter(<span style="color:#a00">$1</span>) } }</code></pre></div></p>
<p>Now we can also do:
<div class="highlight"><pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-swift" data-lang="swift"><span style="color:#00a">let</span> <span style="color:#a00">t</span> = TodoItem(text: <span style="color:#a50">&#34;Buy Beer&#34;</span>).alter([.due(Date.tomorrow.noon), .completed(<span style="color:#00a">true</span>)])</code></pre></div>
Now we can collect any user input encoded as <code>Change</code> in an array and pass that to <code>alter(_ changes: )</code>.</p>
<h2 id="journaling">Journaling</h2>
<p>But let&rsquo;s take it a bit further: let&rsquo;s add journaling — which is rather simple as we just need to keep track of the intermediate items that will be created for every changed value.</p>
<p>As now <code>TodoItem</code>s will refer to each other, we need to wrap the <code>previous</code> TodoItem, because: reasons.
A simple array will do.</p>
<div class="highlight"><pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-swift" data-lang="swift"><span style="color:#00a">private</span> <span style="color:#00a">let</span> <span style="color:#a00">_previous</span>: [TodoItem]
<span style="color:#00a">var</span> <span style="color:#a00">previous</span>: TodoItem? { _previous.first }</code></pre></div>
<p>We change the private init and <code>alter(_ change: )</code> to:</p>
<div class="highlight"><pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-swift" data-lang="swift"><span style="color:#00a">private</span> <span style="color:#00a">init</span> (<span style="color:#00a">_</span> text: <span style="color:#0aa">String</span>, <span style="color:#00a">_</span> completed: <span style="color:#0aa">Bool</span>, <span style="color:#00a">_</span> id: UUID, <span style="color:#00a">_</span> dueDate:Date?, <span style="color:#00a">_</span> creationDate: Date, <span style="color:#00a">_</span> alterDate:Date?, <span style="color:#00a">_</span> previous: TodoItem?) {
    <span style="color:#aaa;font-style:italic">// ..</span>
    <span style="color:#00a">self</span>._previous = previous != <span style="color:#00a">nil</span> ? [previous!] : []
}
<span style="color:#00a">private</span> <span style="color:#00a">func</span> <span style="color:#0a0">alter</span>(<span style="color:#00a">_</span> change:Change) -&gt; TodoItem {
    <span style="color:#00a">let</span> <span style="color:#a00">alterDate</span> = Date()
    <span style="color:#00a">switch</span> change {
    <span style="color:#00a">case</span> .text     (<span style="color:#00a">let</span> <span style="color:#a00">text</span>     ): <span style="color:#00a">return</span> TodoItem( text, completed, id, dueDate, creationDate, alterDate, <span style="color:#00a">self</span>)
    <span style="color:#00a">case</span> .completed(<span style="color:#00a">let</span> <span style="color:#a00">completed</span>): <span style="color:#00a">return</span> TodoItem( text, completed, id, dueDate, creationDate, alterDate, <span style="color:#00a">self</span>)
    <span style="color:#00a">case</span> .id       (<span style="color:#00a">let</span> <span style="color:#a00">id</span>       ): <span style="color:#00a">return</span> TodoItem( text, completed, id, dueDate, creationDate, alterDate, <span style="color:#00a">self</span>)
    <span style="color:#00a">case</span> .due      (<span style="color:#00a">let</span> <span style="color:#a00">dueDate</span>  ): <span style="color:#00a">return</span> TodoItem( text, completed, id, dueDate, creationDate, alterDate, <span style="color:#00a">self</span>)
    }
}</code></pre></div>
<p>let&rsquo;s try it:
<div class="highlight"><pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-swift" data-lang="swift"><span style="color:#00a">let</span> <span style="color:#a00">dd</span> = Date.tomorrow.noon

<span style="color:#00a">var</span> <span style="color:#a00">todo</span> = TodoItem(text: <span style="color:#a50">&#34;Buy Beer&#34;</span>).alter(.due(dd), .text(<span style="color:#a50">&#34;Buy Beer!!&#34;</span>))
todo = todo.alter(.due(dd.dayAfter))
todo = todo.alter(.completed(<span style="color:#00a">true</span>))
<span style="color:#00a">for</span> a <span style="color:#00a">in</span> todo.history {
    print(a)
}</code></pre></div></p>
<p>Result:</p>
<div class="highlight"><pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-fallback" data-lang="fallback">Buy Beer!!		completed:YES		creationdate:2020-05-01 06:16:35.9630		duedate:2020-05-03 12:00:00.0000		alterdate:2020-05-01 06:16:36.0050
Buy Beer!!		completed:NO		creationdate:2020-05-01 06:16:35.9630		duedate:2020-05-03 12:00:00.0000		alterdate:2020-05-01 06:16:36.0040
Buy Beer!!		completed:NO		creationdate:2020-05-01 06:16:35.9630		duedate:2020-05-02 12:00:00.0000		alterdate:2020-05-01 06:16:35.9900
Buy Beer		completed:NO		creationdate:2020-05-01 06:16:35.9630		duedate:2020-05-02 12:00:00.0000		alterdate:2020-05-01 06:16:35.9630
Buy Beer		completed:NO		creationdate:2020-05-01 06:16:35.9630		duedate:No due date						alterdate:No altered
</code></pre></div><p>It shows us the history from the most current version to the earliest version of the item.</p>
<p>But I don&rsquo;t like the idea of having meta information, like previous and alterDate at the same level as the business information. let&rsquo;s move those alterations into a child object <code>Alteration</code>:</p>
<div class="highlight"><pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-swift" data-lang="swift"><span style="color:#00a">struct</span> <span style="color:#0a0;text-decoration:underline">TodoItem</span>: <span style="color:#0aa">Equatable</span>, <span style="color:#0aa">Hashable</span>, CustomDebugStringConvertible {

    <span style="color:#00a">enum</span> <span style="color:#0a0;text-decoration:underline">Change</span> {
        <span style="color:#00a">case</span> text(<span style="color:#0aa">String</span>)
        <span style="color:#00a">case</span> completed(<span style="color:#0aa">Bool</span>)
        <span style="color:#00a">case</span> id(UUID)
        <span style="color:#00a">case</span> due(Date?)
    }

    <span style="color:#00a">private</span> <span style="color:#00a">struct</span> <span style="color:#0a0;text-decoration:underline">Alteration</span>: <span style="color:#0aa">Equatable</span>, <span style="color:#0aa">Hashable</span> {
        <span style="color:#00a">init</span>(<span style="color:#00a">_</span> date: Date, <span style="color:#00a">_</span> id: <span style="color:#0aa">Int</span>, <span style="color:#00a">_</span> previous: TodoItem?) {
            <span style="color:#00a">self</span>.date = date
            <span style="color:#00a">self</span>.id = id
            <span style="color:#00a">self</span>._previous = previous != <span style="color:#00a">nil</span> ? [previous!] : []
        }
        <span style="color:#00a">let</span> <span style="color:#a00">date</span>: Date
        <span style="color:#00a">let</span> <span style="color:#a00">id</span>: <span style="color:#0aa">Int</span>
        <span style="color:#00a">var</span> <span style="color:#a00">previous</span>: TodoItem? { _previous?.first }
        <span style="color:#00a">private</span> <span style="color:#00a">let</span> <span style="color:#a00">_previous</span>: [TodoItem]?
    }

    <span style="color:#00a">let</span> <span style="color:#a00">text</span>: <span style="color:#0aa">String</span>
    <span style="color:#00a">let</span> <span style="color:#a00">completed</span>: <span style="color:#0aa">Bool</span>
    <span style="color:#00a">let</span> <span style="color:#a00">id</span>: UUID
    <span style="color:#00a">let</span> <span style="color:#a00">dueDate</span>: Date?
    <span style="color:#00a">var</span> <span style="color:#a00">creationDate</span>: Date { alteration.previous?.creationDate ?? alteration.date }
    <span style="color:#00a">private</span> <span style="color:#00a">let</span> <span style="color:#a00">alteration</span>: Alteration

    <span style="color:#00a">init</span>(text: <span style="color:#0aa">String</span>) {
        <span style="color:#00a">self</span>.<span style="color:#00a">init</span>(text, <span style="color:#00a">false</span>, UUID(), <span style="color:#00a">nil</span>, Alteration(Date(), <span style="color:#099">0</span>, <span style="color:#00a">nil</span>))
    }

    <span style="color:#00a">init</span>(text: <span style="color:#0aa">String</span>, <span style="color:#00a">_</span> changes:  Change...) { <span style="color:#00a">self</span>.<span style="color:#00a">init</span>(text: text, changes) }
    <span style="color:#00a">init</span>(text: <span style="color:#0aa">String</span>, <span style="color:#00a">_</span> changes: [Change]  ) {
        <span style="color:#00a">let</span>               <span style="color:#a00">intermediate</span> = TodoItem(text:text).alter(changes)
        <span style="color:#00a">self</span>.text       = intermediate.text
        <span style="color:#00a">self</span>.completed  = intermediate.completed
        <span style="color:#00a">self</span>.id         = intermediate.id
        <span style="color:#00a">self</span>.dueDate    = intermediate.dueDate
        <span style="color:#00a">self</span>.alteration = Alteration(intermediate.creationDate, <span style="color:#099">0</span>, <span style="color:#00a">nil</span>)
    }

    <span style="color:#00a">func</span> <span style="color:#0a0">alter</span>(<span style="color:#00a">_</span> changes: Change...) -&gt; TodoItem { alter(changes) }
    <span style="color:#00a">func</span> <span style="color:#0a0">alter</span>(<span style="color:#00a">_</span> changes: [Change] ) -&gt; TodoItem { changes.reduce(<span style="color:#00a">self</span>) { <span style="color:#a00">$0</span>.alter(<span style="color:#a00">$1</span>, alteration.id + <span style="color:#099">1</span>) } }

    <span style="color:#00a">var</span> <span style="color:#a00">history</span>: [TodoItem] { [<span style="color:#00a">self</span>] + (alteration.previous != <span style="color:#00a">nil</span> ? alteration.previous!.history : []) }

    <span style="color:#00a">var</span> <span style="color:#a00">debugDescription</span>: <span style="color:#0aa">String</span> {
        [
            (<span style="color:#a50">&#34;completed&#34;</span>   , completed ? <span style="color:#a50">&#34;YES&#34;</span> : <span style="color:#a50">&#34;NO&#34;</span>),
            (<span style="color:#a50">&#34;creationdate&#34;</span>, df.string(from: creationDate)),
            (<span style="color:#a50">&#34;duedate&#34;</span>     , dueDate != <span style="color:#00a">nil</span>    ? df.string(from:   dueDate!)      : <span style="color:#a50">&#34;No due date</span><span style="color:#a50">\t\t\t\t</span><span style="color:#a50">&#34;</span>),
            (<span style="color:#a50">&#34;alterdate&#34;</span>   , alteration.id &gt; <span style="color:#099">0</span> ? df.string(from: alteration.date) : <span style="color:#a50">&#34;not altered&#34;</span>)
        ].reduce(text) {
            <span style="color:#a00">$0</span> + <span style="color:#a50">&#34;</span><span style="color:#a50">\t\t</span><span style="color:#a50">\(</span><span style="color:#a00">$1</span>.<span style="color:#099">0</span><span style="color:#a50">)</span><span style="color:#a50">:</span><span style="color:#a50">\(</span><span style="color:#a00">$1</span>.<span style="color:#099">1</span><span style="color:#a50">)</span><span style="color:#a50">&#34;</span>
        }
    }

    <span style="color:#aaa;font-style:italic">// </span><span style="color:#00a;font-style:italic">MARK:</span><span style="color:#aaa;font-style:italic"> - private</span>

    <span style="color:#00a">private</span> <span style="color:#00a">init</span> (<span style="color:#00a">_</span> text: <span style="color:#0aa">String</span>, <span style="color:#00a">_</span> completed: <span style="color:#0aa">Bool</span>, <span style="color:#00a">_</span> id: UUID, <span style="color:#00a">_</span> dueDate:Date?, <span style="color:#00a">_</span> alteration: Alteration) {
        <span style="color:#00a">self</span>.text = text
        <span style="color:#00a">self</span>.completed = completed
        <span style="color:#00a">self</span>.id = id
        <span style="color:#00a">self</span>.dueDate = dueDate
        <span style="color:#00a">self</span>.alteration = alteration
    }

    <span style="color:#00a">private</span> <span style="color:#00a">func</span> <span style="color:#0a0">alter</span>(<span style="color:#00a">_</span> change:Change, <span style="color:#00a">_</span> alterID: <span style="color:#0aa">Int</span>) -&gt; TodoItem {
        <span style="color:#00a">let</span> <span style="color:#a00">alterDate</span> = Date()
        <span style="color:#00a">switch</span> change {
        <span style="color:#00a">case</span> .text     (<span style="color:#00a">let</span> <span style="color:#a00">text</span>     ): <span style="color:#00a">return</span> TodoItem( text, completed, id, dueDate, Alteration(alterDate, alterID, <span style="color:#00a">self</span>) )
        <span style="color:#00a">case</span> .completed(<span style="color:#00a">let</span> <span style="color:#a00">completed</span>): <span style="color:#00a">return</span> TodoItem( text, completed, id, dueDate, Alteration(alterDate, alterID, <span style="color:#00a">self</span>) )
        <span style="color:#00a">case</span> .id       (<span style="color:#00a">let</span> <span style="color:#a00">id</span>       ): <span style="color:#00a">return</span> TodoItem( text, completed, id, dueDate, Alteration(alterDate, alterID, <span style="color:#00a">self</span>) )
        <span style="color:#00a">case</span> .due      (<span style="color:#00a">let</span> <span style="color:#a00">dueDate</span>  ): <span style="color:#00a">return</span> TodoItem( text, completed, id, dueDate, Alteration(alterDate, alterID, <span style="color:#00a">self</span>) )
        }
    }
}</code></pre></div>
<p>Note, that I also added to public inits <code>init(text: String, _ changes:  Change...)</code> &amp; <code>init(text: String, _ changes: [Change])</code>, which will use an intermediate item to create a item with those values but discarding the history — I guess this is as close as we can get with this approach to get an interface familiar with what we are used in OOP.</p>
<p><div class="highlight"><pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-swift" data-lang="swift"><span style="color:#00a">let</span> <span style="color:#a00">t</span> = TodoItem(text: <span style="color:#a50">&#34;Buy beer&#34;</span>, .due(dd), .text(<span style="color:#a50">&#34;Buy beer!!&#34;</span>), .due(dd.dayAfter), .completed(<span style="color:#00a">true</span>))</code></pre></div>
<code>t</code> will not have any history bound to it. Besides that, it is equivalent to:
<div class="highlight"><pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-swift" data-lang="swift"><span style="color:#00a">var</span> <span style="color:#a00">todo</span> = TodoItem(text: <span style="color:#a50">&#34;Buy beer&#34;</span>)
todo = todo.alter([.due(dd), .text(<span style="color:#a50">&#34;Buy beer!!&#34;</span>)])
todo = todo.alter(.due(dd.dayAfter))
todo = todo.alter(.completed(<span style="color:#00a">true</span>))</code></pre></div></p>
<p>Also note, that <code>Alteration</code> keeps an <code>id</code>. The idea here is that this allows you to undo stuff, as we know that alterations with the same id on the same item where committed by the user at the same time.</p>
<h2 id="discussion">Discussion</h2>
<p>Please feel free to <a href="https://gitlab.com/vikingosegundo/vikingosegundo.gitlab.io/-/issues/1">discuss this article</a>.</p>

      </div>


      <footer>
        

<section class="see-also">
  
    
    
    
      <h3>See also in CoreUI</h3>
      <nav>
        <ul>
        
        
          
            <li>
              <a href="/posts/agile-architecture-06-example/">Architecture &amp; Agility: 06 - Example Feature implementation</a>
            </li>
          
        
          
            <li>
              <a href="/posts/agile-architecture-05-bdd/">Architecture &amp; Agility: 05 - BDD</a>
            </li>
          
        
          
            <li>
              <a href="/posts/agile-architecture-04-immutable-state/">Architecture &amp; Agility: 04 - Immutable State</a>
            </li>
          
        
          
            <li>
              <a href="/posts/agile-architecture-03-appcore/">Architecture &amp; Agility: 03 - AppCore &amp; UI</a>
            </li>
          
        
          
            <li>
              <a href="/posts/agile-architecture-02-features/">Architecture &amp; Agility: 02 - Features</a>
            </li>
          
        
          
            <li>
              <a href="/posts/agile-architecture-01-usecases/">Architecture &amp; Agility: 01 - UseCases</a>
            </li>
          
        
        </ul>
      </nav>
    
  
</section>


        
        
        
      </footer>
    </article>

    
  </section>

      </div>

      <footer class="footer">
  <section class="container">
    
    
      
        © 2020
      
       Manuel Meyer 
    
    
    
  </section>
</footer>

    </main>

    

    

  </body>

</html>
